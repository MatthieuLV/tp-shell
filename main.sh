#!/usr/bin/env bash
source logs.sh

# Ask to user how old he is
age() {
  echo "How old are you ?"
  read age
  re='^[0-9]+$'
  if ! [[ $age =~ $re ]] ; then
    echo "error: Not a number"; exit 1
  elif [ $age -ge 18 ]; then
    echo "You are an adult"
  else
    echo "Child"
  fi
}

# Ask current password
changepasswd() {
  echo "Please enter your current password :"
  read -s loginpass
  if [[ "$loginpass" = "$PASSWORD" ]]; then
    checkpasswd
  else
    changepasswd
  fi
}

# Ask a new password 
checkpasswd() {
  echo "Please enter a new password :"
  read -s newpass
  echo "Please confirm the new password :"
  read -s newpassconfirm
  if [[ "$newpass" = "$newpassconfirm" ]]; then
    sed -i.bak "s/^\PASSWORD=.*/\PASSWORD=\"${newpass}\"/" ./logs.sh
  else
    checkpasswd
  fi
}

# Ask to user where the html he want to download has to be written
httpget() {
  echo "File name :"
  read filename
  wget ${1} -O - > ${filename}
}

# Contain the prompt logic
cmd() {
  echo "Please enter your password :"
  read -s loginpass
  
  # if the given password is the good one
  if [[ "$loginpass" = "$PASSWORD" ]]; then
    echo "Welcome !"

    # while user don't exit, wait for a new entry
    while [[ 1 ]]; do
      printf "${BLUE} Matt ${NC}${RED} $(date +%R) ${NC}${GREEN} $(pwd) \$${NC} "
      read cmd arg1
      case "$cmd" in
        # name of the command ) function called
        help ) echo -e $HELP;;
        ls ) ls -al $arg1;;
        rm ) rm $arg1;;
        rmd | rmdir ) rm -rf $arg1;;
        about ) echo "This program is a little magic prompt";;
        version | --v | vers ) echo "v1.0.0";;
        age ) age;;
        quit | exit ) exit;;
        profile ) echo "First name :" $FIRST_NAME ", Last name : " $LAST_NAME ", Age :" $AGE ", Email :" $EMAIL;;
        passw ) changepasswd;;
        cd ) cd $arg1;;
        pwd ) pwd;;
        hour ) date +%R;;
        httpget ) httpget $arg1;;
        smtp ) echo "To do";;
        open ) vim $arg1;;
        clear ) clear;;
        * ) echo "command not found";;
      esac
    done
  else
    # if a wrong password is given, ask again
    cmd
  fi
}

# main
main() {
  cmd
}

# call main which itself call cmd
main