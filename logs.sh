#!/usr/bin/env bash
FIRST_NAME="Matthieu"
LAST_NAME="LE VERGER"
AGE="23"
EMAIL="mattlv@hotmail.fr"
PASSWORD="oui"
RED='\033[1;41m'
GREEN='\033[1;42m'
YELLOW='\033[1;43m'
BLUE='\033[1;44m'
NC='\033[0m'
HELP="\n\nhelp : qui indiquera les commandes que vous pouvez utiliser\n\n
ls : lister des fichiers et les dossiers visible comme caché\n\n
rm : supprimer un fichier\n\n
rmd ou rmdir : supprimer un dossier\n\n
about : une description de votre programme\n\n
version ou --v ou vers :  affiche la version de votre prompt\n\n
age : vous demande votre âge et vous dit si vous êtes majeur ou mineur\n\n
quit : permet de sortir du prompt\n\n
profile : permet d’afficher toutes les informations sur vous même.
First Name, Last name, age, email\n\n
passw : permet de changer le password avec une demande de confirmation\n\n
cd : aller dans un dossier que vous venez de créer ou de revenir à un dossier 
précédent\n\n
pwd : indique le répertoire actuelle courant\n\n
hour : permet de donner l’heure actuelle\n\n
httpget : permet de télécharger le code source html d’une page web et de l’enregistrer dans un fichier spécifique. Votre prompt doit vous demander quel sera le nom du fichier.\n\n
smtp : vous permet d’envoyer un mail avec une adresse un sujet et le corp du mail\n\n
open : ouvrir un fichier directement dans l’éditeur VIM même si le fichier n’existe pas"