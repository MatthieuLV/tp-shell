# TP-Shell

## Technos
- Bash

## Objective

Realise a prompt in bash script

## Commands

- help: which will indicate the commands you can use
- ls: list files and folders visible as hidden
- rm: delete a file
- rmd or rmdir: delete a folder
- about: a description of your program
- version or --v or vers: display the version of your prompt
- age: asks you your age and tells you if you are major or minor
- quit: allows to exit the prompt
- profile: allows you to display all the information about yourself.
First Name, Last name, age, email
- passw: allows you to change the password with a confirmation request
- cd: go to a folder you just created or go back to a folder
previous
- pwd: indicates the current current directory
- hour: allows you to give the current hour
- *: indicate an unknown command
- httpget: allows you to download the html source code of a web page and save it in a specific file. Your prompt should ask you what the name of the file will be.
- smtp (not available yet) : allows you to send an email with an address, a subject and the body of the email
- open: open a file directly in the VIM editor even if the file does not exist

## Install

1. Get sources

    ```bash
    git clone git@gitlab.com:MatthieuLV/tp-shell.git
    ```  
   
2. Open project in terminal

3. Start the prompt

    ```bash
    $ ./main.sh
    ```  
   
4. Enter password (oui)

5. Enter commands